<?php


namespace Operators;


class Numbers
{
    public static function sum($number1, $number2)
    {
        return $number1 + $number2;
    }

    public static function subtract($number1, $number2)
    {
        return $number1 - $number2;
    }

    public static function isOdd($number)
    {
        return $number%2 > 0;
    }

    public static function isEven($number)
    {
        return $number%2 === 0;
    }
}